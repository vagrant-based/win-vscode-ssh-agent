# win-vscode-ssh-agent

Script to **add git hosts for plink** to be able **to use git ssh capabilities** by vscode / system widely, like git clone over SSH. Also add necessary environment variable(s).

Ideally combine with [`pageant` - an SSH authentication agent for PuTTY and Plink](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)
to hold you SSH keys, available system-widely in Windows OS.

**You must to run the script as Administrator!**

## Configuration

### git_hosts.conf

`git_hosts.conf` contains list of git hosts. Check for examples, edit to your needs.

Defaults are:

```bash
git@github.com
git@gitlab.com
```

### plink

In script you are able to define the **path of plink** executable. Default value is: `c:\SSH\plink.exe`

## Run

Clone the repository && execute `plink_for_vscode.ps1` as an Administrator.

## Contribution

Feel free to make MR, add powershell linting, propose a new way or functionality.

## Author

Peter Mikaczo
e-mail: <peter.mikaczo@gmail.com>
