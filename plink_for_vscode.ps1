﻿#Requires -RunAsAdministrator

#Name of the system environment variable
$env_var_name= "GIT_SSH"
#Path of the plink executable
$plink="c:\SSH\plink.exe"

if (Test-Path $plink) {   
    Write-Host "plink Folder and file exists"
}
else
{ 
    #PowerShell Create directory if not exists
    $plink_dir = Split-Path -Path $plink -Parent
    if (Test-Path $plink_dir) {   
        Write-Host "Folder exists."
    }
    else
    { 
    New-Item $plink_dir -ItemType Directory
    Write-Host "Folder Created successfully"
}
#download plink
Write-Host "Download missing plink."
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]'Tls,Tls11,Tls12'
Invoke-WebRequest -Uri 'https://the.earth.li/~sgtatham/putty/latest/w64/plink.exe' -OutFile $plink
}

#Set GIT_SSH Environment variable
[System.Environment]::SetEnvironmentVariable($env_var_name,$plink,[System.EnvironmentVariableTarget]::Machine)
[System.Environment]::SetEnvironmentVariable($env_var_name,$plink,[System.EnvironmentVariableTarget]::User)

#for loop (add hosts)
Write-Host "Adding configured git hosts..."
foreach ($line in Get-Content .\git_hosts.conf) {
    Write-Host "Try to add $line"
    echo y|plink $line
}

Write-Host "Get ssh host keys to verify added hosts."
REG QUERY HKEY_CURRENT_USER\Software\SimonTatham\PuTTY\SshHostKeys
